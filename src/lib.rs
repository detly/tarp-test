pub fn run_thing() {
    // Create an async subprocess future.
    let async_exec = async move {
        println!("Spawning first process");

        let mut async_child = async_process::Command::new("sleep")
            .arg("1s")
            .stdout(async_process::Stdio::piped())
            .spawn()
            .unwrap();

        println!("Awaiting first process");

        async_child.status().await.unwrap();

        println!("Spawning second process");

        let mut async_child = async_process::Command::new("sleep")
            .arg("1s")
            .stdout(async_process::Stdio::piped())
            .spawn()
            .unwrap();

        println!("Awaiting second process");

        async_child.status().await.unwrap();

        println!("Future complete");
    };

    // Create the executor and scheduler to run it in Calloop.
    let (exec, sched) = calloop::futures::executor().unwrap();

    // Schedule the future to run in the event loop.
    sched.schedule(async_exec).unwrap();

    let mut event_loop = calloop::EventLoop::<calloop::LoopSignal>::try_new().unwrap();

    // Add the executor to the event loop, and make it stop the loop as soon as
    // it finishes.
    event_loop
        .handle()
        .insert_source(exec, |_, _, stopper| {
            stopper.stop();
        })
        .unwrap();

    // Run the loop.
    event_loop
        .run(None, &mut event_loop.get_signal(), |_| {})
        .unwrap();
}
