const DELAY: std::time::Duration = std::time::Duration::from_millis(100);

fn main() {
    use std::thread::{park, sleep};

    println!("1 (one)");
    sleep(DELAY);
    println!("2 (two)");
    sleep(DELAY);
    println!("3 (three)");

    loop {
        park()
    }
}
